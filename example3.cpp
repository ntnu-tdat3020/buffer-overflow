#include <iostream>

using namespace std;

/**
 * Example showing stack buffer overrun that is detected through AddressSanitizer
 * at runtime (CMakeLists.txt contains: -fsanitize=address).
 *
 * Choose Compile and Run in the Project menu.
 **/
int main() {
  char str[5] = {'t', 'e', 's', 't', '\0'};

  for (size_t c = 0; c <= 7; ++c)
    cout << str[c];
  cout << endl;
}
