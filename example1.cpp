#include <iostream>

using namespace std;

/// Example showing stack buffer overrun that might be detected by the compiler
int main() {
  char str[5] = {'t', 'e', 's', 't', '\0'};

  auto chr = str[7];

  cout << chr << endl;
}
